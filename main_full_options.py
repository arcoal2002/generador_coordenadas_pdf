# imports del sistema
import io
import os
import json
import sys
import math
import time

import keyboard

# imports de watchdog <daemon>
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

# import report lab <herramienta pdf>
from reportlab.lib.units import mm
from reportlab.pdfgen import canvas as can
from PyPDF2 import PdfWriter, PdfReader

# import cv2 <crear ventana>
from pdf2image import convert_from_path
import cv2
import tkinter as tk
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from tkinter import StringVar

# import abrir el navegador
from selenium import webdriver

# ---------------------------GLOBALES-------------------------------
# DRIVER = webdriver.Chrome()
window = tk.Tk()
DATA_DIR = "C:\\Users\\Usuario\\Desktop\\wsoft\\formFiller\\Data\\"
DATA_UB = "./Data/"
DATA_FILE = "solicitud.txt"
FILE_PDF = ''
OUTPUT_NAME = 'nuevoarch'
CAMPO = ""
DATO = ""
Y_ORIGINAL = 0
ESCALA = 1
FLAG = 0
TOTAL_PAGES = 0
GLOBAL_PAGE = 0
FONT_SIZE = 12
FONT_FAMILY = "Helvetica-Bold"


# ---------------------------CODIGO-----------------------------------


def escritor(canvas, dict, current):
    canvas.setFillColorRGB(0 / 255, 0 / 255, 0 / 255)
    canvas.setFont(FONT_FAMILY, FONT_SIZE)
    for val in dict:
        arr = dict[val]
        if arr[0] == current:
            canvas.drawString(int(arr[1]) * mm, int(arr[2]) * mm, arr[3])


def get_pdf_size(page):
    with open(FILE_PDF, 'rb') as file:
        reader = PdfReader(file)
        box = reader.pages[page].mediabox
        arr = [int(box.width) / mm, int(box.height) / mm]
        global Y_ORIGINAL
        Y_ORIGINAL = int(box.height) / mm
        # print(arr)
        return arr


def create_pdf(dict):
    for current in range(TOTAL_PAGES):
        buffer = io.BytesIO()
        with open(FILE_PDF, 'rb') as file:
            # crear un objeto PDFReader para el archivo
            pdf_reader = PdfReader(file, strict=False)
            pdf_writer = PdfWriter()
            # print(type(pdf_reader.pages[current]))
            page = pdf_reader.pages[current]
            canvas = can.Canvas(buffer)
            escritor(canvas, dict, current)
            canvas.save()
            buffer.seek(0)
            new_pdf = PdfReader(buffer, strict=False)
            output = PdfWriter()
            page.merge_page(new_pdf.pages[0])
            pdf_writer.add_page(page)
            output.add_page(new_pdf.pages[0])
            output_file_name = OUTPUT_NAME + str(current) + ".pdf"
            with open(output_file_name, "wb") as out_file:
                # output.write(out_file)
                pdf_writer.write(out_file)
                print(f'El archivo se ah generado correctamente')
                ubicacion = os.getcwd()
                print("Revisa en " +ubicacion+" los archivos llamados <nuevoarch>")


def starter():
    name = DATA_UB + DATA_FILE
    file = open(name).read()
    dict = json.loads(file)
    create_pdf(dict)


def round_half_up(n, decimals=0):
    multiplier = 10 ** decimals
    return math.floor(n * multiplier + 0.5) / multiplier


def json_modifier(page, x, y, field, content):
    temp_file = []
    or_src = open(DATA_UB + DATA_FILE, "r")
    or_src = or_src.readlines()
    size = len(or_src)
    for data in range(size - 1):
        temp_file.append(or_src[data])
    os.rename(DATA_UB + DATA_FILE, DATA_UB + "old_sol.txt")
    file = open(DATA_UB + DATA_FILE, "w")
    for i in range(len(temp_file)):
        if i == len(temp_file) - 1:
            string = temp_file[i].replace("]", "],")
            file.write(string)
        else:
            file.write(temp_file[i])
    str = '        "' + field + '":[' + page + ',' + x + ',' + y + ',"' + content + '"]\n'
    # print(str)
    file.write(str)
    file.write("}")
    file.close()
    os.remove(DATA_UB + "old_sol.txt")
    # os.system("notepad.exe "+DATA_UB+DATA_FILE)


def entry_window():
    def devolverDatos():
        if FLAG == "0":
            messagebox.showinfo("Recordatorio", "Recuerda cerrar la ventana de Menú para que se guarde el documento")
        textoCaja = entryTexto.get()
        texto.set(textoCaja)
        root.destroy()

    root = Tk()
    root.title('Ingresar datos:')

    # FRAME DE ENTRADA DE DATOS

    miFrame = Frame(root)
    miFrame.pack()

    texto = StringVar()

    # ENTRY
    label = Label(miFrame,text='"etiqueta,valor" la etiqueta y \n el valor se separan por coma(,) ')
    label.grid(row=0,column=0,padx=2,pady=5)
    entryTexto = Entry(miFrame, justify=CENTER, textvariable=texto, width=40)
    entryTexto.grid(row=1, column=0, padx=2, pady=5, columnspan=9)
    root.iconbitmap()  # Puedes poner un 'logo' personalizado si lo deseas
    root.geometry(center_screen(root, 300, 120))

    # BOTÓN ACEPTAR

    botonAceptar = Button(miFrame, text="Aceptar", command=lambda: devolverDatos())
    botonAceptar.grid(row=2, column=3, sticky="e", padx=5, pady=5)

    root.mainloop()
    return texto.get()


def texto():
    value = entry_window()
    value = value.split(",")
    return value


def dibujando(event, x, y, flags, param):
    new_mm = 0.2646
    if ESCALA < 1:
        a = round_half_up(1 - ESCALA, 2)
        b = a / ESCALA
        newConstant = 1 + b
        xx = str(round(x * new_mm * newConstant))
        yy = str(round(Y_ORIGINAL - (y * new_mm * newConstant)))
    else:
        xx = str(round(x * new_mm))
        yy = str(round(Y_ORIGINAL - (y * new_mm)))

    if event == cv2.EVENT_LBUTTONDOWN:
        # print("-------------------")
        # print('x=', x)
        # print('y=', y)
        # print('xx=', xx)
        # print('yy=', yy)
        value = texto()
        if value[0] == "":
            pass
        else:
            # '.(' + xx + "," + yy + ")"
            # cv2.putText(image, "." , (x, y), 2, 0.4, (0, 255, 255), 1, cv2.LINE_AA)
            # arr = input("campo,valor\n").split(",")
            json_modifier(str(GLOBAL_PAGE), xx, yy, value[0], value[1])
            window_menu("continuar")


def render():
    pixel = 3.7795
    size = get_pdf_size(0)
    width = math.ceil(size[0] * pixel)
    height = math.ceil(size[1] * pixel)
    cv2.namedWindow('Imagen')
    image = cv2.imread('./images/page' + str(GLOBAL_PAGE) + '.jpg')
    width = math.ceil(width * ESCALA)
    height = math.ceil(height * ESCALA)
    image = cv2.resize(image, (width, height))
    cv2.setMouseCallback('Imagen', dibujando)
    while True:
        cv2.imshow('Imagen', image)
        if cv2.waitKey(1) & 0xFF == 27:
            break
    cv2.destroyAllWindows()


# --------------------------------PARTE VISUAL-----------------------------------


def center_screen(window, x, y):
    wtotal = window.winfo_screenwidth()
    htotal = window.winfo_screenheight()
    pwidth = round(wtotal / 2 - x / 2)
    pheight = round(htotal / 2 - y / 2)
    return str(x) + "x" + str(y) + "+" + str(pwidth) + "+" + str(pheight)


def close():
    keyboard.press_and_release("escape")
    dir = os.getcwd()
    os.system("taskkill /F /IM python.exe /T")


def sizeup():
    global ESCALA
    ESCALA = ESCALA + 0.1
    render()


def sizedown():
    global ESCALA
    ESCALA = ESCALA - 0.1
    render()


def nextpage():
    global GLOBAL_PAGE
    newpage = GLOBAL_PAGE + 1
    if newpage >= TOTAL_PAGES:
        messagebox.showwarning("Advertencia", "No hay mas paginas por mostrar")
    else:
        GLOBAL_PAGE = newpage
    # DRIVER.get("file:///C:/Users/Usuario/Desktop/wsoft/formFiller/" + OUTPUT_NAME + str(GLOBAL_PAGE) + ".pdf")
    render()


def prevpage():
    global GLOBAL_PAGE
    newpage = GLOBAL_PAGE - 1
    if newpage < 0:
        messagebox.showwarning("Advertencia", "No hay mas paginas por mostrar")
    else:
        GLOBAL_PAGE = newpage

    # DRIVER.get("file:///C:/Users/Usuario/Desktop/wsoft/formFiller/" + OUTPUT_NAME + str(GLOBAL_PAGE) + ".pdf")
    render()


def crear():
    starter()
    messagebox.showinfo("","PDF creado con exito en la carpeta del programa\n con el/los nombres de nuevoarch")


def continuar():
    file = open("./data.txt", "r")
    opciones = file.readlines()
    global DATA_DIR
    DATA_DIR = opciones[0].replace("\n", "")
    global FILE_PDF
    FILE_PDF = opciones[1].replace("\n", "")
    # DRIVER.get("file:///C:/Users/Usuario/Desktop/wsoft/formFiller/" + OUTPUT_NAME+str(GLOBAL_PAGE)+".pdf")
    global FLAG
    FLAG = opciones[2].replace("\n", "")
    window.destroy()
    messagebox.showinfo("Recordatorio",
                        "Recuerda no ejecutar multiples acciones a la vez\n puede ocacionar que el programa deje de funcionar")

    pixel = 3.7795
    size = get_pdf_size(0)
    width = math.ceil(size[0] * pixel)
    height = math.ceil(size[1] * pixel)
    images = convert_from_path(FILE_PDF, poppler_path='./poppler-23.01.0/Library/bin', size=(width, height))
    for i in range(len(images)):
        images[i].save('./images/page' + str(i) + '.jpg', 'JPEG')

    initial_count = 0
    directory = "./images/"
    for path in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, path)):
            initial_count += 1
    global TOTAL_PAGES
    TOTAL_PAGES = initial_count

    window_menu("continuar")


def nuevo():
    new_dir = os.getcwd() + "\\Data\\"
    filename = filedialog.askopenfilename()
    dir = "./images"
    if filename:
        for f in os.listdir(dir):
            os.remove(os.path.join(dir, f))
        file = open("./data.txt", "w")
        new_file = filename
        file.write(new_dir + "\n" + new_file+"\n 0")
        file.close()
        window.destroy()
        file = open("./data.txt", "r")
        opciones = file.readlines()
        global DATA_DIR
        DATA_DIR = opciones[0].replace("\n", "")
        global FILE_PDF
        FILE_PDF = opciones[1].replace("\n", "")
        messagebox.showinfo("Recordatorio",
                            "Recuerda no ejecutar multiples acciones a la vez\n puede ocacionar que el programa deje de funcionar")
        pixel = 3.7795
        size = get_pdf_size(0)
        width = math.ceil(size[0] * pixel)
        height = math.ceil(size[1] * pixel)
        images = convert_from_path(FILE_PDF, poppler_path='./poppler-23.01.0/Library/bin', size=(width, height))
        for i in range(len(images)):
            images[i].save('./images/page' + str(i) + '.jpg', 'JPEG')

        initial_count = 0
        directory = "./images/"
        for path in os.listdir(directory):
            if os.path.isfile(os.path.join(directory, path)):
                initial_count += 1
        global TOTAL_PAGES
        TOTAL_PAGES = initial_count

        window_menu("nuevo")
    else:
        messagebox.showwarning("Error de seleccion", "No has seleccionado un archivo")


def window_main():
    window.geometry(center_screen(window, 300, 500))
    window.resizable(0, 0)
    window.title("Menú principal")
    Label(window, text="Escritor pdf", font=("Arial", 14)).pack()
    imagen = PhotoImage(file="menu-image.gif")
    Label(window, image=imagen, bd=0).pack()
    btn_continuar = tk.Button(
        window,
        text="Continuar",
        command=continuar,
        width=15
    )
    btn_nuevo = tk.Button(
        window,
        text="Nuevo archivo",
        command=nuevo,
        width=15
    )
    btn_continuar.place(x=100, y=300)
    btn_nuevo.place(x=100, y=400)
    window.mainloop()


def window_menu(parameter):

    if parameter == "continuar":
        # starter()
        pass
    else:
        file = open(DATA_UB + DATA_FILE, "w")
        file.write("{\n" + "}")
        file.close()
        # DRIVER.get("file:///C:/Users/Usuario/Desktop/wsoft/formFiller/" + OUTPUT_NAME+str(GLOBAL_PAGE)+".pdf")

    window2 = tk.Tk()
    window2.geometry(center_screen(window2, 300, 300))
    window2.resizable(0, 0)
    window2.title("menú")
    boton_pequeño = tk.Button(
        window2,
        text="MAS PEQUEÑA",
        command=sizedown, width=15
    )
    boton_grande = tk.Button(
        window2,
        text="MAS GRANDE",
        command=sizeup, width=15
    )
    boton_cerrar = tk.Button(
        window2,
        text="CERRAR",
        command=close, width=15
    )
    boton_next = tk.Button(
        window2,
        text=">>",
        command=nextpage, width=15
    )
    boton_prev = tk.Button(
        window2,
        text="<<",
        command=prevpage, width=15
    )
    boton_crear = tk.Button(
        window2,
        text="¡CREAR PDF!",
        command=crear, width=15
    )

    boton_grande.place(x=100, y=30)
    boton_pequeño.place(x=100, y=60)
    boton_next.place(x=175, y=150)
    boton_prev.place(x=10, y=150)
    boton_cerrar.place(x=100, y=220)
    boton_crear.place(x=100, y=90)
    recordatorio = tk.Label(window2,text="Recuerda que antes de ejecutar una accion debes de\n presionar esc en la ventana donde está el documento")
    recordatorio.place(x=10,y=250)
    window2.mainloop()
    render()


class MyEventHandler(FileSystemEventHandler):
    def on_modified(self, event):
        print(event)
        # starter()
        # DRIVER.refresh()


if __name__ == '__main__':
    window_main()
"""    observer = Observer()
    observer.schedule(MyEventHandler(), DATA_DIR, recursive=False)
    observer.start()
    window_main()
    try:
        while observer.is_alive():
            time.sleep(10)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()"""
