# GENERADOR DE COORDENADAS PDF

## Requisitos

El primer requisito necesario es contar con Python3 la instalacion puede ser realizada desde la microsoft store o desde la pagina oficial

Para usar este programa es necesario los siguientes modulos instalados:

-keyboard(responsable de que el programa se cierre de manera correcta)

-reportlab(creador de pdf, es necesario una cuenta para instalarlo desde la pagina)

-pypdf2(I/O de pdfs)

-pdf2image(Transforma los PDFs a imagenes)

-cv2(Encargado de obtener las coordenadas de las imagenes)

Adicionalmente existen modulos que ya vienen instalados en python nativamente, en caso contrario es necesario instalarlos manualmente.

## Advertencias

El programa está hecho de manera en que el gestor grafico, es decir la ventana donde seleccionas las coordenadas del pdf, consume una canitidad de recursos considerables debido a la naturaleza misma del programa, es recomendable no tener la ventana abierta todo el tiempo, y que antes de realizar una actividad en la ventana menú seleccionar la ventana de nombre "Imagen" y presionar la tecla "escape" o "esc", las alertas se pueden eliminar cambiando el dato "0" por un 1 en el documento de nombre "data.txt"

## Como ejecutarlo

ir a la carpeta desde consola y escribir el siguiente comando

### "<python.exe> main.py"

donde python.exe es el PATH de python, este puede ser:

### -py
### -python
### -python3
entre otros

se agregaron archivos .bat en los cuales solamente será necesario hacer doble click para ejecutarlo